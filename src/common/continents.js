export default async function continents() {
  const baseURL =
    "https://geoenrich.arcgis.com/arcgis/rest/services/World/geoenrichmentserver/Geoenrichment/countries?f=pjson";

  try {
    let res = await fetch(baseURL);
    let data = await res.json();
    let continents = Array.from(
      new Set(data.countries.map(({ continent }) => continent))
    );
    return continents;
  } catch (error) {
    console.log(error);
  }
}
