// global variables that check if username contains the special characters
let no_symbol_check = 0;

export default function userNameChecker(data) {
  const special = new RegExp("(?=.*[!@#$%&*])");

  // performing the checks

  no_symbol_check = special.test(data) ? 0 : 1;

  if (no_symbol_check === 1 && data.length > 0) return true;
  //return true if the username has no speial characters
  else return false;
}
