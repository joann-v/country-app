// global variables that check if password fulfils conditions
let no_symbol_check = 0;
let password_length_check = 0;

export default function passwordChecker(data) {
  // regular expressions for the different conditions
  const special = new RegExp("(?=.*[!@#$%&*])");

  // performing the checks

  no_symbol_check = special.test(data) ? 0 : 1;
  password_length_check = data.length > 0 && data.length <= 15 ? 1 : 0;

  if (no_symbol_check && password_length_check === 1) return true;
  // returns true if password has no special char and length < 15
  else return false;
}
