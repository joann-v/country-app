import * as React from "react";
import { styled } from "@mui/material/styles";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import CardActions from "@mui/material/CardActions";
import Collapse from "@mui/material/Collapse";
import IconButton from "@mui/material/IconButton";
import Typography from "@mui/material/Typography";
import DeleteIcon from "@mui/icons-material/Delete";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import Flag from "react-world-flags";
import Box from "@mui/material/Box";
import { AuthContext } from "../../components/authcontext";
import {
  getUserfromLS,
  updateExistingUser,
} from "../../services/localstorage.service";

const ExpandMore = styled((props) => {
  const { expand, ...other } = props;
  return <IconButton {...other} />;
})(({ theme, expand }) => ({
  transform: !expand ? "rotate(0deg)" : "rotate(180deg)",
  marginLeft: "auto",
  transition: theme.transitions.create("transform", {
    duration: theme.transitions.duration.shortest,
  }),
}));

export default function FavouriteCard(props) {
  const [expanded, setExpanded] = React.useState(false);
  const { user, removeFavoriteCountries } = React.useContext(AuthContext);

  const removeFavorite = (countryToRemove) => {
    // Get the existing data & parse it
    let existing = getUserfromLS(user.email);
    let updatedCountries = existing.favoriteCountries;
    updatedCountries = updatedCountries.filter((country) => {
      if (country.name !== countryToRemove) {
        return country;
      }
    });
    existing.favoriteCountries = updatedCountries;
    // Save back to localStorage
    updateExistingUser(user.email, JSON.stringify(existing));
    //updated user context
    removeFavoriteCountries(updatedCountries);
  };

  const handleExpandClick = () => {
    setExpanded(!expanded);
  };

  return (
    <Card sx={{ maxWidth: 345, m: 3 }}>
      <Box className="flagbox">
        <Flag code={props.country.id} className="flag" />
      </Box>

      <CardContent>
        <Typography variant="h5">{props.country.name}</Typography>
      </CardContent>
      <CardActions disableSpacing>
        <IconButton
          aria-label="delete from favorites"
          onClick={() => removeFavorite(props.country.name)}
        >
          <DeleteIcon />
        </IconButton>

        <ExpandMore
          expand={expanded}
          onClick={handleExpandClick}
          aria-expanded={expanded}
          aria-label="show more"
        >
          <ExpandMoreIcon />
        </ExpandMore>
      </CardActions>
      <Collapse in={expanded} timeout="auto" unmountOnExit>
        <CardContent>
          <Typography color="text.secondary">Country Code</Typography>
          <Typography sx={{ mb: 1.5 }} variant="body2">
            {props.country.id}
          </Typography>
          <Typography color="text.secondary">Currency Name</Typography>
          <Typography sx={{ mb: 1.5 }} variant="body2">
            {props.country.currencyName}
          </Typography>
          <Typography color="text.secondary">Currency Code</Typography>
          <Typography sx={{ mb: 1.5 }} variant="body2">
            {props.country.currencyCode}
          </Typography>
        </CardContent>
      </Collapse>
    </Card>
  );
}
