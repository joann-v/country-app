import * as React from "react";
import CssBaseline from "@mui/material/CssBaseline";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import Container from "@mui/material/Container";
import AppBar from "../../components/appbar.component";
import Grid from "@mui/material/Grid";
import { AuthContext } from "../../components/authcontext";
import FavouriteCard from "./favouritecard";
import Pagination from "../../components/pagination.component";

export default function FavouriteScreen() {
  const { user } = React.useContext(AuthContext);

  let favoriteElements;
  if (user.favoriteCountries) {
    favoriteElements = user.favoriteCountries.map((country) => {
      return (
        <Grid item xs={12} sm={6} md={4} key={country.id}>
          <FavouriteCard country={country} />
        </Grid>
      );
    });
  }

  let paginationElements;
  if (user.favoriteCountries) {
    paginationElements = (
      <Pagination countries={user.favoriteCountries} isDashboard={false} />
    );
  }

  let noFavourites;
  if (user.favoriteCountries.length === 0)
    noFavourites = (
      <Typography variant="h5" align="center" color="text.secondary" paragraph>
        You have no favourites!
      </Typography>
    );

  return (
    <>
      <CssBaseline />
      <AppBar />
      <main>
        {/* Hero unit */}
        <Box
          sx={{
            bgcolor: "background.paper",
            pt: 6,
            pb: 2,
          }}
        >
          <Container maxWidth="sm">
            <Typography
              component="h1"
              variant="h2"
              align="center"
              color="text.primary"
              gutterBottom
            >
              Your Favourites
            </Typography>
            {noFavourites}
          </Container>
        </Box>
        <Container sx={{ mb: 4, py: 8 }} maxWidth="lg">
          <Grid container spacing={4}>
            {paginationElements}
          </Grid>
        </Container>
      </main>
    </>
  );
}
