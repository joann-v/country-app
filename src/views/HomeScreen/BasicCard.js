import * as React from "react";
import { styled } from "@mui/material/styles";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import CardActions from "@mui/material/CardActions";
import Collapse from "@mui/material/Collapse";
import IconButton from "@mui/material/IconButton";
import Typography from "@mui/material/Typography";
import FavoriteIcon from "@mui/icons-material/Favorite";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import FavoriteBorderIcon from "@mui/icons-material/FavoriteBorder";
import Flag from "react-world-flags";
import Box from "@mui/material/Box";
import { AuthContext } from "../../components/authcontext";
import {
  getUserfromLS,
  updateExistingUser,
} from "../../services/localstorage.service";

const ExpandMore = styled((props) => {
  const { expand, ...other } = props;
  return <IconButton {...other} />;
})(({ theme, expand }) => ({
  transform: !expand ? "rotate(0deg)" : "rotate(180deg)",
  marginLeft: "auto",
  transition: theme.transitions.create("transform", {
    duration: theme.transitions.duration.shortest,
  }),
}));

export default function BasicCard(props) {
  const [expanded, setExpanded] = React.useState(false);
  const { user, addFavoriteCountries } = React.useContext(AuthContext);
  const [isFavorite, setIsFavorite] = React.useState(undefined);
  React.useEffect(() => {
    // set isFavourite as true for all the favourites in db
    for (let i = 0; i < user.favoriteCountries.length; i++) {
      if (user.favoriteCountries[i].name === props.country.name) {
        //console.log(user.favoriteCountries[i].name);
        setIsFavorite(true);
      }
    }
  }, []);

  function addFavorite() {
    // Get the existing data & parse it
    let alreadyExist = false;
    let existing = getUserfromLS(user.email);
    //check if the currently favourited country is part of favourite list
    for (let i = 0; i < existing.favoriteCountries.length; i++) {
      if (existing.favoriteCountries[i].name === props.country.name) {
        alreadyExist = true;
      }
    }
    //if its not part of the favourite list, add it to list
    if (!alreadyExist) {
      existing.favoriteCountries.push(props.country);
      addFavoriteCountries(props.country);
      setIsFavorite(true);
    }
    //console.log(existing.favoriteCountries);
    //console.log(user.favoriteCountries);
    // Save back to localStorage
    updateExistingUser(user.email, JSON.stringify(existing));
  }

  const handleExpandClick = () => {
    setExpanded(!expanded);
  };

  return (
    <Card sx={{ maxWidth: 345, m: 3 }}>
      <Box className="flagbox">
        <Flag code={props.country.id} className="flag" />
      </Box>

      <CardContent>
        <Typography variant="h5">{props.country.name}</Typography>
      </CardContent>
      <CardActions disableSpacing>
        {isFavorite ? (
          <IconButton aria-label="add to favorites" disabled>
            <FavoriteIcon color="error" />
          </IconButton>
        ) : (
          <IconButton aria-label="add to favorites" onClick={addFavorite}>
            <FavoriteBorderIcon />
          </IconButton>
        )}

        <ExpandMore
          expand={expanded}
          onClick={handleExpandClick}
          aria-expanded={expanded}
          aria-label="show more"
        >
          <ExpandMoreIcon />
        </ExpandMore>
      </CardActions>
      <Collapse in={expanded} timeout="auto" unmountOnExit>
        <CardContent>
          <Typography color="text.secondary">Country Code</Typography>
          <Typography sx={{ mb: 1.5 }} variant="body2">
            {props.country.id}
          </Typography>
          <Typography color="text.secondary">Currency Name</Typography>
          <Typography sx={{ mb: 1.5 }} variant="body2">
            {props.country.currencyName}
          </Typography>
          <Typography color="text.secondary">Currency Code</Typography>
          <Typography sx={{ mb: 1.5 }} variant="body2">
            {props.country.currencyCode}
          </Typography>
        </CardContent>
      </Collapse>
    </Card>
  );
}
