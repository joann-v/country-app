import * as React from "react";
import ListItemButton from "@mui/material/ListItemButton";
import ListItemIcon from "@mui/material/ListItemIcon";
import ListItemText from "@mui/material/ListItemText";
import DashboardContent from "./dashboardcontent";
import { CountryContext } from "../../components/countrycontext.components";

export default function Dashboard() {
  const { countries } = React.useContext(CountryContext);

  const [currentContinent, setCurrentContinent] =
    React.useState("All Continents");
  let continentElements;
  const [allContinents, setAllContinents] = React.useState(null);
  React.useEffect(() => {
    let tempContinents = Array.from(
      new Set(countries.map(({ continent }) => continent))
    );
    tempContinents.unshift("All Continents");
    setAllContinents(tempContinents);
  }, [countries]);

  if (allContinents) {
    continentElements = allContinents.map((continent) => {
      if (currentContinent === continent) {
        return (
          <ListItemButton
            key={continent}
            onClick={() => setCurrentContinent(continent)}
            sx={{ backgroundColor: "#43aa8b" }}
          >
            <ListItemIcon>
              {continent.substring(0, 3).toUpperCase()}
            </ListItemIcon>
            <ListItemText primary={continent} />
          </ListItemButton>
        );
      }
      return (
        <ListItemButton
          key={continent}
          onClick={() => setCurrentContinent(continent)}
        >
          <ListItemIcon>{continent.substring(0, 3).toUpperCase()}</ListItemIcon>
          <ListItemText primary={continent} />
        </ListItemButton>
      );
    });
  }

  const [chosenCountries, setChosenCountries] = React.useState(null);

  React.useEffect(() => {
    let tempCountries;
    if (currentContinent === "All Continents") {
      tempCountries = countries;
    } else {
      tempCountries = countries.filter(
        (country) => country.continent === currentContinent
      );
    }
    setChosenCountries(tempCountries);
  }, [currentContinent]);

  //console.log(countries);

  return (
    <DashboardContent
      continentElements={continentElements}
      countries={chosenCountries}
      currentContinent={currentContinent}
    />
  );
}
