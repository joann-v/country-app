import * as React from "react";
import Avatar from "@mui/material/Avatar";
import Button from "@mui/material/Button";
import CssBaseline from "@mui/material/CssBaseline";
import TextField from "@mui/material/TextField";
import Grid from "@mui/material/Grid";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import Container from "@mui/material/Container";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import Select from "@mui/material/Select";
import passwordChecker from "../../common/passwordChecker";
import userNameChecker from "../../common/userNameChecker";
import { AuthContext } from "../../components/authcontext";
import { Link, useNavigate } from "react-router-dom";
import emailValidator from "../../common/emailValidator";
import countries from "../../common/countries";
import {
  getUserfromLS,
  updateUserinLS,
} from "../../services/localstorage.service";

export default function RegistrationScreen() {
  let navigate = useNavigate();

  const { login } = React.useContext(AuthContext);

  let countryElements;
  const [allCountries, setAllCountries] = React.useState(null);
  React.useEffect(() => {
    let active = true;
    load();
    return () => {
      active = false;
    };
    async function load() {
      setAllCountries(undefined); // this is optional
      const res = await countries();
      if (!active) {
        return;
      }
      setAllCountries(res.countries);
    }
  }, []);

  if (allCountries) {
    countryElements = allCountries.map((country) => {
      return (
        <MenuItem value={country.name} key={country.id}>
          {country.name}
        </MenuItem>
      );
    });
  }

  const [country, setCountry] = React.useState("");
  const handleChange = (event) => {
    setCountry(event.target.value);
  };

  const [isEmailValid, setIsEmailValid] = React.useState(false);
  const checkEmailValid = (event) => {
    const t = emailValidator(event.target.value);
    setIsEmailValid(t);
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    const data = new FormData(event.currentTarget);
    const pcheck = passwordChecker(data.get("password"));
    const ucheck = userNameChecker(data.get("userName"));
    let isPasswordConfirmed = false;
    if (data.get("password") === data.get("confirmpassword")) {
      isPasswordConfirmed = true;
    }

    if (pcheck && ucheck && isEmailValid && isPasswordConfirmed) {
      if (getUserfromLS(data.get("email")) === null) {
        updateUserinLS(
          data.get("email"),
          data.get("password"),
          data.get("userName"),
          data.get("country")
        );

        //Setting up user context
        login(data.get("email"));
        //navigate to home page
        navigate("/home");
      } else {
        alert("User already exists!");
      }
    } else {
      if (!pcheck && !ucheck) {
        alert("Password & username should be changed!");
      } else if (!pcheck) {
        alert(
          "Password should have max 15 characters and no special characters!"
        );
      } else if (!ucheck) {
        alert("Username should not contain special characters!");
      } else if (!isPasswordConfirmed) {
        alert("Passwords did not match!");
      }
    }
  };

  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <Box
        sx={{
          marginTop: 8,
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
        }}
      >
        <Avatar sx={{ m: 1, bgcolor: "secondary.main" }}></Avatar>
        <Typography component="h1" variant="h5">
          Register
        </Typography>
        <Box component="form" noValidate onSubmit={handleSubmit} sx={{ mt: 3 }}>
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <TextField
                required
                fullWidth
                id="email"
                label="Email Address"
                name="email"
                autoComplete="email"
                onChange={checkEmailValid}
              />
              {!isEmailValid && (
                <Typography color="error">Please enter valid email</Typography>
              )}
            </Grid>

            <Grid item xs={12}>
              <TextField
                required
                fullWidth
                id="userName"
                label="User Name"
                name="userName"
                autoComplete="family-name"
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                required
                fullWidth
                name="password"
                label="Password"
                type="password"
                id="password"
                autoComplete="new-password"
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                required
                fullWidth
                name="confirmpassword"
                label="Confirm Password"
                type="password"
                id="confirmpassword"
                autoComplete="new-password"
              />
            </Grid>
            <Grid item xs={12}>
              <FormControl fullWidth>
                <InputLabel id="demo-simple-select-label">Country</InputLabel>
                <Select
                  labelId="demo-simple-select-label"
                  id="demo-simple-select"
                  name="country"
                  value={country}
                  label="Country"
                  onChange={handleChange}
                >
                  {countryElements}
                </Select>
              </FormControl>
            </Grid>
          </Grid>
          <Button
            type="submit"
            fullWidth
            variant="contained"
            sx={{ mt: 3, mb: 2 }}
          >
            Submit
          </Button>
          <Grid container justifyContent="flex-end">
            <Grid item>
              <Link to="/login" variant="body2">
                Already have an account? Login
              </Link>
            </Grid>
          </Grid>
        </Box>
      </Box>
    </Container>
  );
}
