import React from "react";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";

export default function PageNotFound() {
  return (
    <Box
      sx={{
        backgroundColor: "primary.main",
      }}
    >
      <Typography
        className="pageNotFound"
        sx={{ color: "white", fontSize: 50 }}
      >
        This Page Does Not Exist!
      </Typography>
    </Box>
  );
}
