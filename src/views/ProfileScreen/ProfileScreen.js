import * as React from "react";
import Avatar from "@mui/material/Avatar";
import Button from "@mui/material/Button";
import CssBaseline from "@mui/material/CssBaseline";
import TextField from "@mui/material/TextField";
import Grid from "@mui/material/Grid";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import Container from "@mui/material/Container";
import AppBar from "../../components/appbar.component";
import { AuthContext } from "../../components/authcontext";
import passwordChecker from "../../common/passwordChecker";
import DeleteButton from "./components/deletebutton";
import ShowDetails from "./components/showdetails";
import {
  getUserfromLS,
  updateExistingUser,
} from "../../services/localstorage.service";

export default function ProfileScreen() {
  const { user, changePassword } = React.useContext(AuthContext);

  const handleSubmit = (event) => {
    event.preventDefault();
    const data = new FormData(event.currentTarget);
    const pcheck = passwordChecker(data.get("password"));
    if (pcheck) {
      // Get the existing data & parse it
      let existing = getUserfromLS(user.email);
      //change password
      existing["password"] = data.get("password");
      // Save back to localStorage
      updateExistingUser(user.email, JSON.stringify(existing));

      //change value in user context
      changePassword(data.get("password"));
    } else {
      alert(
        "Password should have max 15 characters and no special characters!"
      );
    }
  };

  return (
    <Box>
      <AppBar />
      <Container component="main" maxWidth="xs">
        <CssBaseline />
        <Box
          sx={{
            marginTop: 8,
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
          }}
        >
          <Avatar sx={{ m: 1, bgcolor: "secondary.main" }}></Avatar>
          <Typography component="h1" variant="h5">
            Your Profile
          </Typography>
          <Box
            component="form"
            noValidate
            onSubmit={handleSubmit}
            sx={{ mt: 3 }}
          >
            <Grid container spacing={2}>
              <Grid item xs={12}>
                <TextField
                  required
                  fullWidth
                  id="email"
                  label="Email Address"
                  name="email"
                  autoComplete="email"
                  value={user.email}
                  disabled
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  disabled
                  id="outlined-disabled"
                  label="User Name"
                  defaultValue={user.userName}
                  sx={{
                    width: "100%",
                  }}
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  disabled
                  id="outlined-disabled second"
                  label="Country"
                  defaultValue={user.country}
                  sx={{
                    width: "100%",
                  }}
                />
              </Grid>
              <Grid item xs={12}>
                <ShowDetails country={user.country} />
              </Grid>

              <Grid item xs={12}>
                <TextField
                  required
                  fullWidth
                  name="password"
                  label="Change Password"
                  type="password"
                  id="password"
                  autoComplete="new-password"
                />
              </Grid>
            </Grid>
            <Button
              type="submit"
              fullWidth
              variant="contained"
              sx={{ mt: 3, mb: 2 }}
            >
              Change Password
            </Button>
            <DeleteButton />
          </Box>
        </Box>
      </Container>
    </Box>
  );
}
