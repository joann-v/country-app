import * as React from "react";
import Button from "@mui/material/Button";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import DialogTitle from "@mui/material/DialogTitle";
import { CountryContext } from "../../../components/countrycontext.components";
import Flag from "react-world-flags";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";

export default function ShowDetails(props) {
  const [open, setOpen] = React.useState(false);
  const { countries } = React.useContext(CountryContext);

  const yourCountry = countries.filter(
    (country) => country.name === props.country
  );
  console.log(yourCountry);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <div>
      <Button
        variant="outlined"
        onClick={handleClickOpen}
        color="primary"
        sx={{ width: "100%" }}
      >
        View details of your country
      </Button>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
        sx={{}}
      >
        <DialogTitle id="alert-dialog-title">{"Details"}</DialogTitle>
        <DialogContent>
          <Box className="flagbox">
            <Flag code={yourCountry[0].id} className="flag" />
          </Box>

          <DialogContentText id="alert-dialog-description">
            <div>
              <Typography variant="h5">{yourCountry[0].name}</Typography>
              <div>
                <Typography color="text.secondary">Country Code</Typography>
                <Typography sx={{ mb: 1.5 }} variant="body2">
                  {yourCountry[0].id}
                </Typography>
                <Typography color="text.secondary">Currency Name</Typography>
                <Typography sx={{ mb: 1.5 }} variant="body2">
                  {yourCountry[0].currencyName}
                </Typography>
                <Typography color="text.secondary">Currency Code</Typography>
                <Typography sx={{ mb: 1.5 }} variant="body2">
                  {yourCountry[0].currencyCode}
                </Typography>
              </div>
            </div>
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose}>Ok</Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
