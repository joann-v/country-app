import * as React from "react";
import Button from "@mui/material/Button";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import DialogTitle from "@mui/material/DialogTitle";
import { AuthContext } from "../../../components/authcontext";
import { useNavigate } from "react-router-dom";
import { deleteAccount } from "../../../services/localstorage.service";

export default function AlertDialog() {
  const [open, setOpen] = React.useState(false);
  const { user, logout } = React.useContext(AuthContext);
  let navigate = useNavigate();

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
    console.log("Close");
  };

  const handleAgree = () => {
    setOpen(false);
    console.log("Agree");
    deleteAccount(user.email);
    logout();
    navigate("/login");
  };

  return (
    <div>
      <Button
        variant="outlined"
        onClick={handleClickOpen}
        color="error"
        sx={{ width: "100%" }}
      >
        Delete Account
      </Button>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">
          {"Are you sure you want to delete the account?"}
        </DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            On deleting the account you will lose the list of all your favourite
            countries. This process is irreversible.
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose}>Disagree</Button>
          <Button onClick={handleAgree} autoFocus color="error">
            Agree
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
