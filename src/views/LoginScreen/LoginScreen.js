import * as React from "react";
import Avatar from "@mui/material/Avatar";
import Button from "@mui/material/Button";
import CssBaseline from "@mui/material/CssBaseline";
import TextField from "@mui/material/TextField";
import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import Typography from "@mui/material/Typography";
import Container from "@mui/material/Container";
import { AuthContext } from "../../components/authcontext";
import { Link, useNavigate } from "react-router-dom";
import {
  lengthofLS,
  getKey,
  getUserfromLS,
} from "../../services/localstorage.service";

export default function LoginScreen() {
  let navigate = useNavigate();

  const { login } = React.useContext(AuthContext);

  const handleSubmit = (event) => {
    event.preventDefault();
    const data = new FormData(event.currentTarget);

    let userExists = false;
    let correctPassword = false;

    for (var i = 0; i < lengthofLS(); i++) {
      let emailKey = getKey(i);
      let user = getUserfromLS(emailKey);
      if (getKey(i) === data.get("email")) {
        userExists = true;
        if (user.password === data.get("password")) {
          correctPassword = true;
          //Setting up user context
          login(data.get("email"));
          //navigate to home page
          navigate("/home");
        }
      }
    }

    if (!userExists) {
      alert("User doesn't exist");
      event.preventDefault();
    } else if (!correctPassword) {
      alert("Incorrect Password!");
      event.preventDefault();
    }
  };

  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <Box
        sx={{
          marginTop: 8,
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
        }}
      >
        <Avatar sx={{ m: 1, bgcolor: "secondary.main" }}></Avatar>
        <Typography component="h1" variant="h5">
          Login
        </Typography>
        <Box component="form" onSubmit={handleSubmit} noValidate sx={{ mt: 1 }}>
          <TextField
            margin="normal"
            required
            fullWidth
            id="email"
            label="Email Address"
            name="email"
            autoComplete="email"
            autoFocus
          />
          <TextField
            margin="normal"
            required
            fullWidth
            name="password"
            label="Password"
            type="password"
            id="password"
            autoComplete="current-password"
          />

          <Button
            type="submit"
            fullWidth
            variant="contained"
            sx={{ mt: 3, mb: 2 }}
          >
            LogIn
          </Button>
          <Grid container>
            <Grid item>
              <Link to="/" variant="body2">
                {"Don't have an account? Sign Up"}
              </Link>
            </Grid>
          </Grid>
        </Box>
      </Box>
    </Container>
  );
}
