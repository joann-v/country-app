import React from "react";
import { fetchapi } from "../services/fetchapi.service";
const CountryContext = React.createContext();

function CountryContextProvider(props) {
  const [countries, setCountries] = React.useState(undefined);
  const [loading, setLoading] = React.useState(true);

  React.useEffect(() => {
    const fetchCountries = async () => {
      try {
        let data = await fetchapi();
        setCountries(data.countries);
        setLoading(false);
      } catch (error) {
        console.log(error);
      }
    };

    fetchCountries();
  }, []);

  return (
    <CountryContext.Provider
      value={{
        countries,
      }}
    >
      {!loading && props.children}
    </CountryContext.Provider>
  );
}

export { CountryContextProvider, CountryContext };
