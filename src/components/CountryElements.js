import React from "react";
import axios from "axios";
import MenuItem from "@mui/material/MenuItem";

export default function CountryElements() {
  const baseURL =
    "https://geoenrich.arcgis.com/arcgis/rest/services/World/geoenrichmentserver/Geoenrichment/countries?f=pjson";

  const [countries, setCountries] = React.useState(null);

  React.useEffect(() => {
    axios.get(baseURL).then((response) => {
      setCountries(response.data.countries);
    });
  }, []);

  console.log(countries);

  if (countries) {
    const countryElements = countries.map((country) => {
      return (
        <MenuItem value={country.name} key={country.id}>
          {country.name}
        </MenuItem>
      );
    });
    return countryElements;
  } else {
    return <></>;
  }
}
