import * as React from "react";
import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import IconButton from "@mui/material/IconButton";
import PersonIcon from "@mui/icons-material/Person";
import FavoriteIcon from "@mui/icons-material/Favorite";
import LogoutIcon from "@mui/icons-material/Logout";
import HomeIcon from "@mui/icons-material/Home";
import { AuthContext } from "./authcontext";
import { useNavigate } from "react-router-dom";

export default function ButtonAppBar() {
  const { user, logout } = React.useContext(AuthContext);
  let navigate = useNavigate();
  const onButtonClick = (location) => {
    navigate("/" + location);
  };
  const onLogoutClick = () => {
    logout();
    navigate("/login");
  };

  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar position="static">
        <Toolbar>
          <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
            Hi {user.userName},
          </Typography>
          <IconButton color="inherit" onClick={() => onButtonClick("home")}>
            <HomeIcon />
          </IconButton>
          <IconButton color="inherit" onClick={() => onButtonClick("profile")}>
            <PersonIcon />
          </IconButton>
          <IconButton
            color="inherit"
            onClick={() => onButtonClick("favourites")}
          >
            <FavoriteIcon />
          </IconButton>
          <IconButton color="inherit" onClick={onLogoutClick}>
            <LogoutIcon />
          </IconButton>
        </Toolbar>
      </AppBar>
    </Box>
  );
}
