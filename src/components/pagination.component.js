import React from "react";
import Grid from "@mui/material/Grid";
import BasicCard from "../views/HomeScreen/basiccard";
import FavouriteCard from "../views/FavouriteScreen/favouritecard";
import Typography from "@mui/material/Typography";
import Container from "@mui/material/Container";
import Button from "@mui/material/Button";
import ButtonGroup from "@mui/material/ButtonGroup";

export default function Pagination(props) {
  const [countries, setCountries] = React.useState(props.countries);
  const [currentPage, setCurrentPage] = React.useState(1);
  const itemsPerPage = 5;

  React.useEffect(() => {
    setCountries(props.countries);
  }, [props.countries]);

  React.useEffect(() => {
    setCurrentPage(1);
  }, [props.countries]);

  //console.log(props.countries);

  function handleClick(event) {
    setCurrentPage(Number(event.target.id));
  }

  // Logic for displaying current items
  const indexOfLastItem = currentPage * itemsPerPage;
  const indexOfFirstItem = indexOfLastItem - itemsPerPage;
  const currentItems = countries.slice(indexOfFirstItem, indexOfLastItem);

  const renderItems = currentItems.map((country) => {
    if (props.isDashboard) {
      return (
        <Grid item xs={12} sm={6} md={4} key={country.id}>
          <BasicCard country={country} />
        </Grid>
      );
    } else {
      return (
        <Grid item xs={12} sm={6} md={4} key={country.id}>
          <FavouriteCard country={country} />
        </Grid>
      );
    }
  });

  // Logic for displaying page numbers
  const pageNumbers = [];
  for (let i = 1; i <= Math.ceil(countries.length / itemsPerPage); i++) {
    pageNumbers.push(i);
  }

  const renderPageNumbers = pageNumbers.map((number) => {
    if (currentPage === number) {
      return (
        <Button key={number} id={number} onClick={handleClick}>
          {number}
        </Button>
      );
    }
    return (
      <Button
        key={number}
        id={number}
        onClick={handleClick}
        sx={{ color: "text.secondary" }}
      >
        {number}
      </Button>
    );
  });

  return (
    <>
      <Grid
        sx={{ display: "flex", flexDirection: "row" }}
        container
        spacing={4}
      >
        {renderItems}
      </Grid>

      <Container
        maxWidth="sm"
        sx={{
          display: "flex",
          flexDirection: "row",
          justifyContent: "center",
        }}
      >
        <Typography variant="body1">
          <ButtonGroup
            variant="text"
            aria-label="text button group"
            className="pagecontainer"
            id="page-numbers"
          >
            {renderPageNumbers}
          </ButtonGroup>
        </Typography>
      </Container>
    </>
  );
}
