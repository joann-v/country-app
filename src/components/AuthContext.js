import React from "react";
import { getUserfromLS } from "../services/localstorage.service";

const AuthContext = React.createContext();

function AuthContextProvider(props) {
  //current user that is logged in
  const [user, setUser] = React.useState({
    email: "",
    password: "",
    userName: "",
    country: "",
    favoriteCountries: [],
  });

  function login(email) {
    const user = getUserfromLS(email);
    setUser(user);
  }

  function logout() {
    setUser({
      email: "",
      password: "",
      userName: "",
      country: "",
      favoriteCountries: [],
    });
  }

  function changePassword(newPassword) {
    setUser((prevUser) => ({ ...prevUser, password: newPassword }));
  }

  function addFavoriteCountries(newCountry) {
    setUser((prevUser) => {
      let newList = prevUser.favoriteCountries;
      newList.push(newCountry);
      return { ...prevUser, favoriteCountries: newList };
    });
  }

  function removeFavoriteCountries(updatedCountries) {
    setUser((prevUser) => {
      return { ...prevUser, favoriteCountries: updatedCountries };
    });
  }

  return (
    <AuthContext.Provider
      value={{
        user,
        login,
        logout,
        changePassword,
        addFavoriteCountries,
        removeFavoriteCountries,
      }}
    >
      {props.children}
    </AuthContext.Provider>
  );
}

export { AuthContextProvider, AuthContext };
