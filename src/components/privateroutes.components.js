import * as React from "react";
import { Navigate, Outlet } from "react-router-dom";
import { AuthContext } from "./authcontext";
import { CountryContextProvider } from "./countrycontext.components";

export default function PrivateRoutes() {
  const { user } = React.useContext(AuthContext);

  return user.email === "" ? (
    <Navigate to={"/login"} />
  ) : (
    <CountryContextProvider>
      <Outlet />
    </CountryContextProvider>
  );
}
