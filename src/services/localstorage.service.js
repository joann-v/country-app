export function getUserFromLS(email) {
  return localStorage.getItem(email);
}

export function updateUserinLS(email, password, userName, country) {
  localStorage.setItem(
    email,
    JSON.stringify({
      email,
      password,
      userName,
      country,
      favoriteCountries: [],
    })
  );
}

export function updateExistingUser(email, userString) {
  localStorage.setItem(email, userString);
}

export function lengthofLS() {
  return localStorage.length;
}

export function getUserfromLS(email) {
  return JSON.parse(localStorage.getItem(email));
}

export function deleteAccount(email) {
  localStorage.removeItem(email);
}

export function getKey(index) {
  return localStorage.key(index);
}
