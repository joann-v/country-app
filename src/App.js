import * as React from "react";
import { createTheme, ThemeProvider } from "@mui/material/styles";
import RegistrationScreen from "./views/RegistrationScreen/registrationscreen";
import LoginScreen from "./views/LoginScreen/loginscreen";
import Dashboard from "./views/HomeScreen/dashboard";
import ProfileScreen from "./views/ProfileScreen/profilescreen";
import FavouriteScreen from "./views/FavouriteScreen/favouritescreen";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import PrivateRoutes from "./components/privateroutes.components";
import PageNotFound from "./views/PageNotFound/pagenotfound";

let theme = createTheme({
  palette: {
    primary: {
      main: "#43aa8b",
    },
    secondary: {
      main: "#003049",
    },
  },
});

theme = createTheme(theme, {
  palette: {
    info: {
      main: theme.palette.secondary.main,
    },
  },
});

function App() {
  return (
    <BrowserRouter>
      <ThemeProvider theme={theme}>
        <div className="App">
          <Routes>
            <Route path="/" element={<RegistrationScreen />} />
            <Route path="login" element={<LoginScreen />} />
            <Route element={<PrivateRoutes />}>
              <Route path="home" element={<Dashboard />} />
              <Route path="profile" element={<ProfileScreen />} />
              <Route path="favourites" element={<FavouriteScreen />} />
            </Route>
            <Route path="*" element={<PageNotFound />} />
          </Routes>
        </div>
      </ThemeProvider>
    </BrowserRouter>
  );
}

export default App;
